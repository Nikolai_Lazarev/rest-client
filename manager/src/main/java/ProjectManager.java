import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class ProjectManager {
	private Logger logger = Logger.getLogger(LocalizationManager.class);
	private Properties properties;
	private static ProjectManager instance;


	private ProjectManager(){
		try {
			properties = new Properties();
			properties.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
		}catch (IOException e){
			logger.error(e);
		}
	}

	public static ProjectManager getInstance(){
		if(instance == null){
			instance = new ProjectManager();
		}
		return instance;
	}

	public String getValue(String key){
		return properties.getProperty(key);
	}
}
