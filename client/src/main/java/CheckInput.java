import org.apache.log4j.Logger;

import java.util.Scanner;

public class CheckInput {

	protected static Scanner scanner = new Scanner(System.in);
	private static Logger logger = Logger.getLogger(CheckInput.class);

	public static int checkInt(String message){
		String input;
		logger.info(message);
		while(!(input = scanner.nextLine()).matches("\\d+") ){
			logger.info(message);
		}
		return Integer.valueOf(input);
	}

	public static String checkString(String message){
		String input;
		logger.info(message);
		input = scanner.nextLine();
		return input;
	}
}
