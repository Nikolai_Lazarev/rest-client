import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.epam.webservices.soap.User;
import kong.unirest.Unirest;
import org.apache.log4j.Logger;

import java.util.Scanner;


public class Main {
	private static Logger logger = Logger.getLogger(Main.class);
	private static ProjectManager manager = ProjectManager.getInstance();
	private static LocalizationManager localizationManager = LocalizationManager.getInstance();
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		String userName;
		String input = "";
		String userPassword;
		ISystemUserInterface systemUserInterface;
		String token = null;
		while (token == null) {
			logger.info(localizationManager.getValue("input.user.name"));
			userName = scanner.nextLine();
			logger.info(localizationManager.getValue("input.user.password"));
			userPassword = scanner.nextLine();
			token = Unirest.post(manager.getValue("auth"))
					.field("username", userName)
					.field("password", userPassword)
					.queryString("username", userName)
					.queryString("password", userPassword)
					.asJson().getBody().getObject().getString("token");
		}
		User user = getUserFromToken(token);
		if(user.ROLE == User.RoleEnum.admin){
			systemUserInterface = new AdministratorInterface(user, token);
		}else{
			systemUserInterface = new UserInterface(user, token);
		}
		systemUserInterface.printCommandList();
		while(!(input = scanner.nextLine()).equalsIgnoreCase("exit")){
			systemUserInterface.doCommand(input);
			systemUserInterface.printCommandList();
		}
	}

	private static User getUserFromToken(String token) {
		DecodedJWT jwt = JWT.decode(token);
		String userRole = jwt.getClaim("role").asString();
		int userId = jwt.getClaim("id").asInt();
		String login = jwt.getClaim("login").asString();
		return new User(userId, login, userRole);
	}
}
