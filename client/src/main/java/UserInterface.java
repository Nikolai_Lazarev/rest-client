import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.epam.webservices.soap.Author;
import com.epam.webservices.soap.Book;
import com.epam.webservices.soap.Bookmark;
import com.epam.webservices.soap.User;
import kong.unirest.GenericType;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.apache.log4j.Logger;

import java.util.List;

public class UserInterface implements ISystemUserInterface {

	private static Logger logger = Logger.getLogger(UserInterface.class);
	protected User root;
	protected String token;


	UserInterface(User user, String token) {
		this.token = token;
		DecodedJWT jwt = JWT.decode(token);
		String userRole = jwt.getClaim("ROLE").asString();
		root = user;
	}

	@Override
	public void printCommandList() {
		logger.info(localization.getValue("client.interface.user.command.list"));
	}

	@Override
	public void doCommand(String input) {
		switch (input) {
			case "1":
				try {
					int isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
					int page = CheckInput.checkInt(localization.getValue("bookmark.input.page"));
					logger.info(addBookmark(new Bookmark(root.USERID, isbn, page)));
				}catch (NumberFormatException e){
					logger.error(e);
				}
				break;
			case "2":
				List<Book> books = getAllBooks();
				if(books != null) {
					for (Book x : books) {
						logger.info(x.toString());
					}
				}else{
					logger.info(localization.getValue("book.get.failure"));
				}
				break;
			case "3":
				List<Bookmark> bookmarks = getAllBookmark();
				if (bookmarks != null) {
					for(Bookmark x : bookmarks){
						logger.info(x);
					}
				}
				break;
			case "4":
				try {
					int isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
					logger.info(deleteBookmark(isbn));
				}catch (NumberFormatException e){
					logger.error(e);
				}
				break;
			case "5":
				getAuthors().forEach(author -> logger.info(author.toString()));
				break;
			case "6":
				try {
					int isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
					Book book = getBookByISBN(isbn);
					if (book!=null){
						logger.info(book.toString());
					}else{
						logger.info(localization.getValue("book.get.failure"));
					}
				}catch (NumberFormatException e){
					logger.error(e);
				}
				break;
			case "7":
				int id = CheckInput.checkInt(localization.getValue("book.input.author.id"));
				books = getBookByAuthor(id);
				if(books!=null){
					for(Book x : books){
						logger.info(x.toString());
					}
				}else {
					logger.info(localization.getValue("book.get.failure"));
				}
			default:
		}

	}


	protected String addBookmark(Bookmark bookmark) {
		JSONObject res =  Unirest.put(manager.getValue("bookmark.add"))
				.routeParam("userid", String.valueOf(bookmark.USERID))
				.queryString(manager.getValue("token"), token)
				.queryString(manager.getValue("books.param.isbn"), bookmark.ISBN)
				.queryString(manager.getValue("bookmark.param.page"), bookmark.PAGE)
				.asJson().getBody().getObject();
		if(!res.has("message")){
			return localization.getValue("user.id") + " " + res.getString("USERID") +"\n"
					+ localization.getValue("book.isbn")+ " " + res.getString("ISBN") + "\n"
					+localization.getValue("book.page") +" " + res.getString("PAGE");
		}else {
			return res.getString("message");
		}
	}


	protected List<Book> getAllBooks() {
		return Unirest.get(manager.getValue("books.get"))
				.queryString(manager.getValue("token"), token)
				.asObject(new GenericType<List<Book>>() {
				}).getBody();
	}

	protected List<Bookmark> getAllBookmark() {
		return Unirest.get(manager.getValue("bookmark.get"))
				.routeParam("userid", String.valueOf(root.USERID))
				.queryString(manager.getValue("token"), token)
				.asObject(new GenericType<List<Bookmark>>() {
				}).getBody();
	}

	protected String deleteBookmark(int isbn) {
		return Unirest.post(manager.getValue("bookmark.delete"))
				.routeParam("userid", String.valueOf(root.USERID))
				.routeParam("bookid", String.valueOf(isbn))
				.queryString(manager.getValue("token"), token)
				.asJson().getBody().getObject().getString("message");

	}

	protected Book getBookByISBN(int isbn) {
		return Unirest.get(manager.getValue("books.get.isbn"))
				.routeParam("id", String.valueOf(isbn))
				.queryString(manager.getValue("token"), token)
				.asObject(Book.class).getBody();
	}

	protected List<Author> getAuthors() {
		return Unirest.get(manager.getValue("authors.get"))
				.queryString(manager.getValue("token"), token)
				.asObject(new GenericType<List<Author>>() {
				}).getBody();
	}
	protected List<Book> getBookByAuthor(int id){
		return Unirest.get(manager.getValue("books.get.by.author"))
				.routeParam("id", String.valueOf(id))
				.queryString(manager.getValue("token"), token)
				.asObject(new GenericType<List<Book>>() {
				}).getBody();
	}
}
