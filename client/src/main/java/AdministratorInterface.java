import com.epam.webservices.soap.Book;
import com.epam.webservices.soap.Bookmark;
import com.epam.webservices.soap.User;
import kong.unirest.GenericType;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Scanner;

public class AdministratorInterface extends UserInterface implements ISystemUserInterface {

	private static Logger logger = Logger.getLogger(AdministratorInterface.class);
	private static Scanner scanner = new Scanner(System.in);

	AdministratorInterface(User user, String token) {
		super(user, token);
		this.token = token;
	}


	@Override
	public void printCommandList() {
		super.printCommandList();
		logger.info(localization.getValue("client.interface.admin.command.list"));
	}

	@Override
	public void doCommand(String input) {
		switch (input) {
			case "8":
				String author = CheckInput.checkString(localization.getValue("book.input.author.name"));
				String publDate = CheckInput.checkString(localization.getValue("book.input.public.date"));
				String bookTitle = CheckInput.checkString(localization.getValue("book.input.title"));
				int isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
				int pageAmount = CheckInput.checkInt(localization.getValue("book.input.page.amount"));
				logger.info(addBook(new Book(isbn, bookTitle, author, publDate, pageAmount)));
				break;
			case "9":
				isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
				logger.info(deleteBook(isbn));
				break;
			case "10":
				String name = (CheckInput.checkString(localization.getValue("user.input.name")));
				logger.info(setAdmin(name));
				break;
			case "11":
				name = (CheckInput.checkString(localization.getValue("user.input.name")));
				logger.info(setUser(name));
				break;
			case "12":
				String password = (CheckInput.checkString(localization.getValue("user.input.password")));
				String login = (CheckInput.checkString(localization.getValue("user.input.name")));
				logger.info(addUser(new User(login, password)));
				break;
			case "13":
				List<User> users = getUsers();
				if (users.size() > 0) {
					users.forEach(user -> logger.info(user.toString()));
				} else {
					logger.info(localization.getValue("user.get.not.found"));
				}
				break;
			case "14":
				isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
				List<Bookmark> bookmarks = getBookmarkByISBN(isbn);
				if (bookmarks.size() > 0) {
					bookmarks.forEach(bookmark -> logger.info(bookmark.toString()));
				} else {
					logger.info(localization.getValue("bookmark.get.failure"));
				}
				break;
			case "15":
				int userid = CheckInput.checkInt(localization.getValue("user.input.id"));
				bookmarks = getUserBookmarks(userid);
				if (bookmarks != null) {
					for (Bookmark x : bookmarks) {
						logger.info(x);
					}
				}
				break;
			case "16":
				name = CheckInput.checkString(localization.getValue("user.input.name"));
				logger.info(deleteUser(name));
			default:
				super.doCommand(input);
		}
	}

	private String addBook(Book book) {//done
		JSONObject res = Unirest.put(manager.getValue("books") + manager.getValue("books.add"))
				.queryString(manager.getValue("books.param.isbn"), book.ISBN)
				.queryString(manager.getValue("books.param.title"), book.TITLEBOOK)
				.queryString(manager.getValue("books.param.author"), book.AUTHOR)
				.queryString(manager.getValue("books.param.publ"), book.PUBLDATE)
				.queryString(manager.getValue("books.param.page"), book.PAGECOUNT)
				.queryString(manager.getValue("token"), token)
				.asJson().getBody().getObject();
		if (!res.has("message")) {
			return localization.getValue("book.add.success");
		} else {
			return res.getString("message");
		}
	}


	private String deleteBook(int isbn) {
		return Unirest.post(manager.getValue("books.delete"))
				.routeParam("id", String.valueOf(isbn))
				.queryString(manager.getValue("token"), token)
				.asJson().getBody().getObject().getString("message");

	}

	private String setAdmin(String name) {
		return Unirest.put(manager.getValue("users.to.admin"))
				.routeParam("name", String.valueOf(name))
				.queryString(manager.getValue("token"), token)
				.asJson().getBody().getObject().getString("message");
	}

	private String setUser(String name) {
		return Unirest.put(manager.getValue("users.to.user"))
				.routeParam("name", String.valueOf(name))
				.queryString(manager.getValue("token"), token)
				.asJson().getBody().getObject().getString("message");
	}


	private String addUser(User user) {
		JSONObject res = Unirest.put(manager.getValue("users.add.user"))
				.queryString(manager.getValue("token"), token)
				.queryString(manager.getValue("users.param.login"), user.LOGIN)
				.queryString(manager.getValue("users.param.password"), user.PASSWORD)
				.asJson().getBody().getObject();
		if (!res.has("message")) {
			return localization.getValue("user.add.success");
		} else {
			return res.getString("message");
		}
	}

	private List<User> getUsers() {
		List<User> users = Unirest.get(manager.getValue("users.get.users"))
				.queryString(manager.getValue("token"), token)
				.asObject(new GenericType<List<User>>() {
				}).getBody();
		return users;
	}

	private List<Bookmark> getBookmarkByISBN(int isbn) {
		return Unirest.get(manager.getValue("bookmark.get.isbn"))
				.routeParam("bookid", String.valueOf(isbn))
				.queryString(manager.getValue("token"), token)
				.asObject(new GenericType<List<Bookmark>>() {
				}).getBody();
	}

	private List<Bookmark> getUserBookmarks(int id) {
		return Unirest.get(manager.getValue("bookmark.get"))
				.routeParam("userid", String.valueOf(id))
				.queryString(manager.getValue("token"), token)
				.asObject(new GenericType<List<Bookmark>>() {
				}).getBody();
	}

	private String deleteUser(String userName) {
		return Unirest.post(manager.getValue("users.delete"))
				.queryString(manager.getValue("token"), token)
				.routeParam("name", userName)
				.asJson().getBody().getObject().getString("message");
	}
}
