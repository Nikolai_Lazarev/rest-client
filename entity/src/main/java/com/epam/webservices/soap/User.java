package com.epam.webservices.soap;

import org.apache.commons.codec.digest.DigestUtils;

public class User extends Entity {
	public int USERID;
	public final String LOGIN;
	public String PASSWORD;
	public final RoleEnum ROLE;

	public User() {
		this.USERID = 0;
		this.LOGIN = null;
		this.PASSWORD = null;
		this.ROLE = RoleEnum.user;
	}

	public User(String login, String password) {
		this.LOGIN = login;
		this.PASSWORD = DigestUtils.md5Hex(password);
		this.ROLE = RoleEnum.user;
	}

	public User(String login, String password, String role) {
		this.LOGIN = login;
		this.PASSWORD = password;
		this.ROLE = RoleEnum.valueOf(role);
	}

	public User(int USERID, String LOGIN, String ROLE) {
		this.USERID = USERID;
		this.LOGIN = LOGIN;
		this.ROLE = RoleEnum.valueOf(ROLE);
	}

	public User(int userid, String login, String password, String role) {
		this.USERID = userid;
		this.LOGIN = login;
		this.PASSWORD = DigestUtils.md5Hex(password);
		this.ROLE = RoleEnum.valueOf(role);
	}


	public enum RoleEnum {
		admin, user, notActive
	}

	@Override
	public String toString() {
		return "userId: " + USERID + "\n" +
				"Login: " + LOGIN + "\n" +
				"role: " + ROLE.toString() + "\n";
	}
}

