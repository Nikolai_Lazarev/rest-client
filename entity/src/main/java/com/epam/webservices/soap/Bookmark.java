package com.epam.webservices.soap;

public class Bookmark extends Entity {
    public final int USERID;
    public final int ISBN;
    public final int PAGE;
    private boolean active = true;

    public Bookmark() {
        this.USERID = 0;
        this.ISBN = 0;
        this.PAGE = 0;
        this.active = true;
    }

    public Bookmark(int userid, int isbn, int page) {
        this.USERID = userid;
        this.ISBN = isbn;
        this.PAGE = page;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "userId" + USERID + "\n" +
                "isbn: " + ISBN + "\n" +
                "page: " + PAGE + "\n" +
                "status" + active + "\n";
    }
}
