package com.epam.webservices.soap;


import java.io.Serializable;

public class Book extends Entity implements Serializable {
    public final int ISBN;
    public final String TITLEBOOK;
    public final String AUTHOR;
    public final String PUBLDATE;
    public final int PAGECOUNT;
    public boolean status;

    public Book() {
        this.ISBN = 0;
        this.TITLEBOOK = null;
        this.AUTHOR = null;
        this.PUBLDATE = null;
        this.PAGECOUNT = 0;
        status = true;
    }

    public Book(int isbn, String titlebook, String author, String publdate, int pagecount) {
        this.ISBN = isbn;
        this.TITLEBOOK = titlebook;
        this.AUTHOR = author;
        this.PUBLDATE = publdate;
        this.PAGECOUNT = pagecount;
        status = true;
    }

    @Override
    public String toString() {
        return "isbn: " + ISBN + "\n"+
                "title: " + TITLEBOOK + "\n"+
                "author: " + AUTHOR +"\n" +
                "publication date: " + PUBLDATE + "\n"+
                "page count: "+ PAGECOUNT + "\n";
    }
}
