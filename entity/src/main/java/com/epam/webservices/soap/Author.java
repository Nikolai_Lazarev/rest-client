package com.epam.webservices.soap;

public class Author extends Entity {
    public int AUTHORID;
    public final String NAME;

    public Author() {
        this.AUTHORID = 0;
        this.NAME = null;
    }

    public Author(String name) {
        this.NAME = name;
    }

    public Author(int authorid, String name) {
        this.AUTHORID = authorid;
        this.NAME = name;
    }

    @Override
    public String toString() {
        return "id: " + AUTHORID + "\n" +
                "name: " + NAME + "\n";
    }
}
